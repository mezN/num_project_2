"""
Numerik 2, Projekt 2,
Aufgabe 2.1.2: Nichtlineare Regression mittels Gauß-Newton Verfahren
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import matplotlib.pyplot as plt
from solver import GaussNewton

# Im folgenden wollen wir einen funktionalen Zusammenhang zwischen Jahres-
# zahlen und gemeldeten Mobiltelefonen herstellen.

# Das Array D enthält den Datensatz, in der ersten Zeile befinden sich Jahreszahlen (X-Werte) 
# und in der zweiten Zeile die dazugehörige Anzahl von gemeldeten Mobiltelefonen (Y-Werte).
D = np.array([[1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
  1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2008, 2009, 2010],
 [340213, 681825, 1230855, 2069441, 3508944, 5283055, 7557148, 11032753,
  16009461, 24134421, 33758661, 44042992, 55312293, 69209321, 86047003, 
  109478031, 128374512, 140766842, 158721981, 182140362, 207896198, 233000000,
  262700000, 276610580, 300520098]], dtype = np.float)

# Wir verschieben die Jahreszahlen, sodass sie bei 0 beginnen.
D[0] = D[0]-1985

# Zusätzlich fügen wir eine Zeile von normalisierten Daten ein.
D = np.append(D, [D[1]*1e-6], axis = 0)

# Mithilfe des Gauß-Newton-Verfahrens können die Nullstellen eines überbestimmten 
# nichtlinearen Gleichungssystems näherungsweise bestimmt werden. Um die Parameter
# unserer Modellfunktion "h(x;a,b) = a*exp(b*x)" zu bestimmen, betrachten wir die 
# nichtlineare Fehlerfunktion "f(a,b) = (y0 - h(x0; a, b), ..., yn - h(yn; a, b))^t", 
# welche mittels Gauß-Newton Verfahren minimiert werden kann. 

# f1 ist die Fehlerfunktion ausgewertet an den nicht normalisierten Daten,
# x ist ein Vektor, dessen Komponenten als a und b interpretiert werden.
f1 = lambda x: (D[1] - x[0]*np.exp(x[1]*D[0]))
# f2 ist die Fehlerfunktion ausgewertet an den normalisierten Daten.
f2 = lambda x: (D[2] - x[0]*np.exp(x[1]*D[0]))
# Die Ableitung, die für das Gauß-Newton-Verfahren benötigt wird, ist für beide Fehlerfunktionen gleich.
df = lambda x: np.array([-np.exp(x[1]*D[0]),(-x[0]*np.exp(x[1]*D[0])*D[0])]).T

# Wir haben auch eine quadratische Fehlerfunktion betrachtet, diese führt allerdings zu 
# keiner beträchtlichen Verbesserung des Ergebnisses. 
#f1 = lambda x: (D[1] - x[0]*np.exp(x[1]*D[0]))**2
#f2 = lambda x: (D[2] - x[0]*np.exp(x[1]*D[0]))**2
#df = lambda x: np.array([-np.exp(x[1]*D[0]),(-x[0]*np.exp(x[1]*D[0])*D[0])]*(D[1] - x[0]*np.exp(x[1]*D[0]))*2).T

x0 = np.array([0,0])
a1,b1 = GaussNewton(f1,df,x0)
a2,b2 = GaussNewton(f2,df,x0)

plt.plot(D[0],a1*np.exp(b1*D[0]), label = "a*exp(b*x)")
plt.plot(D[0],a2*np.exp(b2*D[0])*1e6, label = "a*exp(b*x), normalisiert")
plt.plot(D[0], D[1], label = "Daten")
plt.legend(loc=2)
plt.show()

"""
 Wie man am Plot unschwer erkennen kann, hat die Normalisierung der Daten keine 
 Auswirkung auf das Ergebnis, die Graphen beider Modellfunktionen liegen direkt
 übereinander.
"""


