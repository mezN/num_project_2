"""
Numerik 2, Projekt 2,
Aufgabe 1.1: Quadratische Konvergenz
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import sys

# Wir untersuchen und veranschaulichen das Konvergenzverhalten unterschiedlicher Verfahren zur
# Nullstellensuche

def f(x):
    """
    Die linke Seite des gegebenen Gleichungssystems; als Funktion R² -> R²
    Eine Nullstelle der Funktion ist eine Lösung des Gleichungssystems
    """
    fx = np.zeros((2,),dtype=np.float)
    fx[0] = x[0]**2 + x[1]**2 + 0.6*x[1] - 0.16
    fx[1] = x[0]**2 - x[1]**2 + x[0] - 1.6*x[1] - 0.14
    return fx

def J(x,h=np.sqrt(sys.float_info.epsilon)):
    """
    Berechne die Jacobimatrix mittels finiter Differenzen
    """
    Jx = np.zeros((2,2),dtype=np.float)
    Jx[0] = (f(x+np.array([h,0]))-f(x))/h
    Jx[1] = (f(x+np.array([0,h]))-f(x))/h
    return Jx.T

if __name__ == "__main__":
    # Die Aufgabe läuft genau gleich ab wie die 1.1.a, mit dem Unterschied,
    # dass wir die Jacobimatrix mittels finiter Differenzen approximieren

    eps = sys.float_info.epsilon
    (x_opt, hist) = s.newton(f,J,np.array([0.6,0.25]),tol=np.sqrt(eps))

    # der erste Plot
    X = np.arange(-0.5, 0.5, 0.003)
    X, Y = np.meshgrid(X, X)
    Z = np.array([la.norm(f(np.array([x,y]))) for x,y in zip(np.ravel(X),np.ravel(Y))])
    Z = Z.reshape(X.shape)

    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    surf = ax.plot_surface(X,Y,Z,cmap=cm.jet,linewidth=0.1,alpha = 0.67)

    X2 = np.array(hist).T[0]
    Y2 = np.array(hist).T[1]
    Z2 = np.array([la.norm(f(np.array([x,y]))) for x,y in zip(np.ravel(X2),np.ravel(Y2))])
    scatter = ax.scatter(X2,Y2,Z2,s = 100,marker = "x",c="r")
    plt.show()

    # der zweite Plot
    err = np.array([la.norm(x_opt-x) for x in hist])
    plt.semilogy(err)
    plt.show()
    # die Konvergenz ist nach wie vor quadratisch
