"""
Numerik 2, Projekt 2,
Aufgabe 1.3.1: Konvergenzbeschleunigung des Newtonverfahrens

@author: Chris Wendler, Franz Luggin, Jona Klemenc 
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
import sys


def f(x):
    """
    Berechnet für den 3x1-Vektor 'x' die in der Angabe gegebenen Gleichungen
    """
    fx = np.zeros((3,),dtype=np.float)
    fx[0] = x[0] + x[0]*x[1] + x[1]**2
    fx[1] = x[0]**2 + x[1]**2 - 2*x[0]
    fx[2] = x[0] + x[2]**2
    return fx
    
def J(x):
    """
    Analytisch berechnete Jacobi Matrix für die gewünschte Stelle.
    """
    Jx = np.zeros((3,3),dtype=np.float)
    Jx[0,0] = 1+x[1]
    Jx[0,1] = x[0]+2*x[1]
    Jx[1,0] = 2*x[0] -2
    Jx[1,1] = 2*x[1]
    Jx[2,0] = 1
    Jx[2,2] = 2*x[2]
    return Jx

if __name__ == "__main__":
    x0 = np.array([0.1,0.5,1])      # gegebener Startwert
    eps = np.sqrt(sys.float_info.epsilon)   # Wurzel aus der Maschinengenauigkeit

    # Siehe solver.py für die Funktionen 'newton' und 'aitken'
    (x_opt, hist) = s.newton(f,J,x0,tol=eps)    # Newtonverfahren
    # Eine Iteration des Newton-Verfahrens für s.aitken
    phi = lambda x: s.newton(f,J,x,maxIter = 1)[0]
    # Mit epsilon-Algorithmus beschleunigtes Verfahren
    (x_opt2,hist2) = s.aitken(phi,x0,tol=eps)
    
    # Konvergenzverhalten, Abstand zur einzigen reellen Lösung (0,0,0) ist gleich der Norm
    err = np.array([la.norm(x) for x in hist])
    err2 = np.array([la.norm(x) for x in hist2])
    # X-Achse: Anzahl der Iterationen
    # Y-Achse: Fehler
    plt.semilogy(err, label = "Newton")
    plt.semilogy(err2, label = "Aitken")
    plt.legend()
    plt.show()
    
    print("Jacobi-Matrix in (0,0,0):\n{}".format(J((0,0,0))))
    
    """
    Wie man im Fehlerplot deutlich erkennen kann, ist das Newton-Verfahren in diesem Fall
    nur linear statt quadratisch konvergent. Der Grund dafür ist die im Nullpunkt singuläre
    Jacobi-Matrix (siehe print). Daher ist der epsilon-Algorithmus gut geeignet, denn lineare
    Konvergenz ist (asymptotisch) geometrisch, die vorausgesetzt wird.
    Die Anzahl der Iterationen für eine bestimmte Genauigkeit ist deutlich niedriger, aber
    eine Iteration auch aufwändiger. Letztendlich ist der epsilon-Algorithmus in diesem 
    Beispiel mehr als doppelt so schnell für gegebene Toleranz (0.628ms / 0.232 ms ~ 2.7, 
    gemessen per %%timeit-Befehl).
    """
    
    
    
    