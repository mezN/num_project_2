"""
Numerik 2, Projekt 2,
Aufgabe 2.1.3: Nichtlineare Regression mittels Gauß-Newton Verfahren - logistisches Wachstum
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import matplotlib.pyplot as plt
from solver import GaussNewton

# Im folgenden wollen wir einen funktionalen Zusammenhang zwischen Jahres-
# zahlen und gemeldeten Mobiltelefonen herstellen.

# Das Array D enthält den Datensatz, in der ersten Zeile befinden sich Jahreszahlen (X-Werte) 
# und in der zweiten Zeile die dazugehörige Anzahl von gemeldeten Mobiltelefonen (Y-Werte).
D = np.array([[1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
  1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2008, 2009, 2010],
 [340213, 681825, 1230855, 2069441, 3508944, 5283055, 7557148, 11032753,
  16009461, 24134421, 33758661, 44042992, 55312293, 69209321, 86047003, 
  109478031, 128374512, 140766842, 158721981, 182140362, 207896198, 233000000,
  262700000, 276610580, 300520098]], dtype = np.float)

# Wir verschieben die Jahreszahlen, sodass sie bei 0 beginnen.
D[0] = D[0]-1985

# Zusätzlich fügen wir eine Zeile von normalisierten Daten ein.
D = np.append(D, [D[1]*1e-9], axis = 0)

# Mithilfe des Gauß-Newton-Verfahrens können die Nullstellen eines überbestimmten 
# nichtlinearen Gleichungssystems näherungsweise bestimmt werden. Um die Parameter
# unserer Modellfunktion "h(x;G,k,c) = G/(1+exp(-k*G*x-c))" zu bestimmen, betrachten wir die 
# nichtlineare Fehlerfunktion "f(a,b) = (y0 - h(x0; a, b), ..., yn - h(yn; a, b))^t", 
# welche mittels Gauß-Newton-Verfahren minimiert werden kann. 

log_growth = lambda G, k, c, x: G / (1 + np.exp(-k*G*x - c))
dfdc = lambda G,k,c,x: G*(1 + np.exp(-k*G*x - c))**(-2)*np.exp(-k*G*x - c)
dfdk = lambda G,k,c,x: G*x*dfdc(G,k,c,x)
dfdG = lambda G,k,c,x: (1 + np.exp(-k*G*x-c))**(-1) + dfdc(G,k,c,x)*k*x

# f1 ist die Fehlerfunktion ausgewertet an den nicht normalisierten Daten, 
# x ist ein Vektor, dessen Komponenten als G, k und c interpretiert werden.
f1 = lambda x: (D[1] - log_growth(x[0], x[1], x[2], D[0]))
df1 = lambda x: -np.array([dfdG(x[0],x[1],x[2],D[0]),dfdk(x[0],x[1],x[2],D[0]),dfdc(x[0],x[1],x[2],D[0])]).T
# f1 ist die Fehlerfunktion ausgewertet an den normalisierten Daten.
f2 = lambda x: (D[2] - log_growth(x[0], x[1], x[2], D[0]))
df2 = lambda x: -np.array([dfdG(x[0],x[1],x[2],D[0]),dfdk(x[0],x[1],x[2],D[0]),dfdc(x[0],x[1],x[2],D[0])]).T

# x1 und x2 sind Startwerte für das Gauß-Newton-Verfahren für die nicht-
# normalisierten und normalisierten Daten.
x1 = np.array([3.5*1e8,1e-9,-5])
x2 = np.array([0.5,0.5,-3])

G1,k1,c1 = GaussNewton(f1,df1,x1,1e-8)
G2,k2,c2 = GaussNewton(f2,df2,x2,1e-8)

plt.plot(D[0], G1/(1 + np.exp(-k1*G1*D[0]-c1)),label="logistisches Wachstum")
plt.plot(D[0], G2/(1 + np.exp(-k2*G2*D[0]-c2))*1e9,label="logistisches Wachstum, normalisiert")
plt.plot(D[0], D[1],label="Daten")
plt.legend(loc=2)
plt.show()

"""
 Bei dieser Aufgabe war besonders auffällig, dass das Gauß-Newton Verfahren 
 äußerst empfindlich auf Änderungen des Startwertes reagiert. Geringe Änderungen 
 an dem von uns experimentell ermittelten Startwert können dramatische Veränderungen
 für das Ergebnis bedeuten. Vermutlich hängt das mit den unterschiedlichen Größen-
 ordnungen der einzelnen Parameter zusammen, wodurch die Norm des Parametervektors
 vom Parameter G dominiert wird.

 Wie man am Plot unschwer erkennen kann, hat die Normalisierung der Daten kaum
 eine Auswirkung auf das Ergebnis, die Graphen beider Modellfunktionen liegen 
 fast übereinander. Allerdings reagiert das Verfahren auf den normalisierten Daten 
 weniger empfindlich auf Veränderungen des Startwertes für den Parameter G.  
"""

