"""
Numerik 2, Projekt 2,
Aufgabe 1.3.2: Konvergenzbeschleunigung des (restarted)-Broydenverfahrens

@author: Chris Wendler, Franz Luggin, Jona Klemenc 
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
import sys


def f(x):
    """
    Berechnet für den 3x1-Vektor 'x' die in der Angabe gegebenen Gleichungen
    """
    fx = np.zeros((3,),dtype=np.float)
    fx[0] = x[0] + x[0]*x[1] + x[1]**2
    fx[1] = x[0]**2 + x[1]**2 - 2*x[0]
    fx[2] = x[0] + x[2]**2
    return fx
    
def J(x):
    """
    Analytisch berechnete Jacobi Matrix für die gewünschte Stelle.
    """
    Jx = np.zeros((3,3),dtype=np.float)
    Jx[0,0] = 1+x[1]
    Jx[0,1] = x[0]+2*x[1]
    Jx[1,0] = 2*x[0] -2
    Jx[1,1] = 2*x[1]
    Jx[2,0] = 1
    Jx[2,2] = 2*x[2]
    return Jx

if __name__ == "__main__":
    x0 = np.array([0.1,0.5,1])      # gegebener Startwert
    eps = np.sqrt(sys.float_info.epsilon)   # Wurzel aus der Maschinengenauigkeit
    # Eine Iteration des Broyden-Verfahrens für s.aitken und das restarted-Broyden Verfahren.
    phi = lambda x: s.broyden(f,J,x,maxIter = 1)[0]
    
    # Die Broyden-Iteration flacht nach einer Iteration ab und verbessert sich nicht mehr
    (x_opt, hist) = s.broyden(f,J,x0,maxIter = 3)
    # Mehrfache Neustarts mit neuem Startwert (berechnet f(x0) und J(x0) neu) für Konvergenz
    for i in range(32):
        x_opt = phi(x_opt)
        hist += [x_opt]
    # Mit epsilon-Algorithmus beschleunigtes Verfahren
    (x_opt2,hist3) = s.aitken(phi,x0,tol=eps)

    err = np.array([la.norm(x) for x in hist])
    err2 = np.array([la.norm(x) for x in hist3])
    # X-Achse: Anzahl der Iterationen
    # Y-Achse: Fehler
    plt.semilogy(err,label="restarted Broyden")
    plt.semilogy(err2,label="Aitken")
    plt.legend()
    plt.show()
    
    print("Näherungslösungen durch Broyden und den Epsilon-Algorithmus:")
    print (x_opt)
    print(x_opt2)
    
    """
    Die Broyden-Iteration flacht nach einer Iteration ab und verbessert sich nicht mehr bis 
    man das Verfahren mit einem neuen Startwert nochmals startet. 
    """
    
    
    
    