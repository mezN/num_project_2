"""
Numerik 2, Projekt 2,
Aufgabe 1.2: Newton Fraktale
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import sys

# Die Aufgabe funktioniert genau gleich wie 1.2.a; mit dem Unterschied dass wir
# anstatt dem Newtonverfahren das gedämpfte Newtonverfahren verwenden.

def f(x):
    """
    die gegebene Funktion
    """
    return x**3-1
    
def J(x):
    """
    Analytisch berechnete Jacobi-Matrix für die gewünschte Stelle.
    """
    return 3*x**2
    
if __name__ == "__main__":
    eps = sys.float_info.epsilon
    rng = np.arange(-1,1,0.01)
    mesh = np.array([[np.complex(x,y) for x in rng] for y in rng])

    #verwende gedämpftes Newton-Verfahren
    x_opts = [s.dampNewton(f,J,x0,np.sqrt(eps),40) for x0 in mesh.flatten()]
    color = []
    for x in x_opts:
        if la.norm(x-1)<1e-3:
            color.append((0,0,1))
        elif la.norm(x-np.complex(np.cos(2*np.pi/3),np.sin(2*np.pi/3)))<1e-3:
            color.append((1,0,0))
        elif la.norm(x-np.complex(np.cos(4*np.pi/3),np.sin(4*np.pi/3)))<1e-3:
            color.append((0,1,0))
        else:
            color.append((0,0,0))
    color=np.array(color).reshape((len(rng),len(rng),3))
    plt.imshow(color,extent=[-1,1,-1,1])
    plt.show()

    # Man sieht, dass die "Überschussbereiche" deutlich schmäler werden, also jene Bereiche,
    # welche zum am weitesten entfernten Nullpunkt konvergieren, da die anderen
    # zwei ähnlich nah sind und das Newtonverfahren schnell sehr weit in Richtung des
    # dritten Nullpunkts läuft.

    # für den Unterschied (|Aufgabe B - Aufgabe A|) siehe figure_diff.png:
    # gelb = grün - rot = rot - grün
    # lila = rot - blau = blau - rot
    # türkis = blau - grün = grün - blau