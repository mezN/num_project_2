# -*- coding: utf-8 -*-
"""
Numerik 2, Projekt 2,
Aufgabe 2.1: Vergleich der Regressionsverfahren
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import matplotlib.pyplot as plt
from solver import GaussNewton

# Das Array D enthält den Datensatz, in der ersten Zeile befinden sich Jahreszahlen (X-Werte) 
# und in der zweiten Zeile die dazugehörige Anzahl von gemeldeten Mobiltelefonen (Y-Werte).
D = np.array([[1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
  1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2008, 2009, 2010],
 [340213, 681825, 1230855, 2069441, 3508944, 5283055, 7557148, 11032753,
  16009461, 24134421, 33758661, 44042992, 55312293, 69209321, 86047003, 
  109478031, 128374512, 140766842, 158721981, 182140362, 207896198, 233000000,
  262700000, 276610580, 300520098]], dtype = np.float)

# Wir verschieben die Jahreszahlen, sodass sie bei 0 beginnen.
D[0] = D[0]-1985

# Damit wir die nicht-lineare Modellfunktion: "y = f(x) = a*exp(b*x)" mittels linearer 
# Regression bestimmen können, logarithmieren wir zunächst die Daten und fügen sie als 
# weitere Zeile in unser Datenarray ein.
D = np.append(D, [np.log(D[1])], axis = 0)
# Zusätzlich fügen wir eine Zeilen mit normalisierten Daten ein 
D = np.append(D, [D[1]*1e-6], axis = 0)
D = np.append(D, [np.log(D[3])], axis = 0)

# Lineare Regression:
A=np.vstack([D[0],np.ones(len(D[0]))]).T
b1,log_a1 = np.linalg.lstsq(A,D[2])[0]
b2,log_a2 = np.linalg.lstsq(A,D[4])[0]
a1=np.exp(log_a1)
a2=np.exp(log_a2)

# Nichtlineare Regression
f1 = lambda x: (D[1] - x[0]*np.exp(x[1]*D[0]))
f2 = lambda x: (D[3] - x[0]*np.exp(x[1]*D[0]))
df = lambda x: np.array([-np.exp(x[1]*D[0]),(-x[0]*np.exp(x[1]*D[0])*D[0])]).T
x0 = np.array([0,0])
a3,b3 = GaussNewton(f1,df,x0)
a4,b4 = GaussNewton(f2,df,x0)

#Lineare Regression vs. Gauß-Newton Verfahren
plt.plot(D[0],a1*np.exp(b1*D[0]), label = "Lineare Regression")
plt.plot(D[0],a3*np.exp(b3*D[0]), label = "Gauß-Newton")
plt.plot(D[0], D[1], label = "Daten")
plt.legend(loc=2)
plt.show()

""" 
 Aus der direkten Gegenüberstellung der beiden Verfahren geht hervor, dass sich 
 das Gauß-Newton Verfahren am rechten Randbereich deutlich besser verhält. Vermutlich 
 liegt das an der Tatsache, dass sich die Größenordnung der Fehler im rechten Randbereich 
 durch das Logarithmieren der Daten, welches für die Anwendung der linearen Regression 
 notwendig ist, stark reduziert.
 
 Der Vergleich von normalisierten und nicht normalisierten Daten geht bereits 
 aus den anderen Dateien hervor.  
"""
##Normalisierte Daten vs. nicht-normalisierte Daten (Newton)
#plt.plot(D[0],a1*np.exp(b1*D[0]), label = "nicht normalisiert")
#plt.plot(D[0],a2*np.exp(b2*D[0])*1e6, label = "normalisiert")
#plt.plot(D[0], D[1], label = "Daten")
#plt.legend(loc=2)
#plt.show()
#
##Normalisierte Daten vs. nicht-normalisierte Daten (Gauß-Newton)
#plt.plot(D[0],a3*np.exp(b3*D[0]), label = "nicht normalisiert")
#plt.plot(D[0],a4*np.exp(b4*D[0])*1e6, label = "normalisiert")
#plt.plot(D[0], D[1], label = "Daten")
#plt.legend(loc=2)
#plt.show()