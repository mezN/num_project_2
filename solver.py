# -*- coding: utf-8 -*-
"""
Numerik 2, Projekt2,
die supertolle Solversammlung

@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import numpy.linalg as la

def newton(f,J,x,tol=1e-8,maxIter=0):
    """
    Newtoniteration
    @param f: betrachtete Funktion
    @param J: Ableitung der betrachteten Funktion
    @param x0: Startwert
    @param tol: Abbruch bei Fehler < tol
    @param maxIter: Abbruch nach maxIter Iterationen
    """
    x0 = x
    dx = la.solve(J(x0),-f(x0))
    x1 = x0 + dx
    hist = [x0,x1]
    i = 1
    while la.norm(x1 - x0) >= tol:
        if maxIter > 0 and i >= maxIter:
            break
        x0 = x1
        dx = la.solve(J(x0),-f(x0))
        x1 = x0 + dx
        hist += [x1]
        i = i+1
    return (x1,hist)
            
def broyden(f,J,x,tol=1e-8,maxIter=0):
    """
    Broyden-Newtoniteration
    @param f: betrachtete Funktion
    @param J: Ableitung der betrachteten Funktion
    @param x0: Startwert
    @param tol: Abbruch bei Fehler < tol
    @param maxIter: Abbruch nach maxIter Iterationen
    """
    x0 = x
    J0 = J(x0)
    dx = la.solve(J0,-f(x0))
    x1 = x0 + dx
    hist = [x0,x1]
    dx = x1-x0
    J1 = J0 + ((f(x1)-f(x0)-J0.dot(x0))/(dx.dot(dx))).dot(dx.T)
    i = 1
    while la.norm(f(x1) - f(x0)) >= tol:
        if maxIter > 0 and i >= maxIter:
            break
        x0 = x1
        J0 = J1
        dx = la.solve(J0,-f(x0))
        x1 = x0 + dx
        J1 = J0 + ((f(x1)-f(x0)-J0.dot(x0))/(dx.dot(dx))).dot(dx.T)
        hist += [x1]
        i = i+1
    return (x1,hist)

def newton1D(f,J,x,tol=1e-8,maxIter=0):
    """
    Newtoniteration
    @param f: betrachtete Funktion
    @param J: Ableitung der betrachteten Funktion
    @param x0: Startwert
    @param tol: Abbruch bei Fehler < tol
    @param maxIter: Abbruch nach maxIter Iterationen
    """
    x0 = x
    dx = -f(x0)/J(x0)
    x1 = x0 + dx
    hist = [x0,x1]
    i = 1
    while la.norm(x1 - x0) >= tol:
        if maxIter > 0 and i >= maxIter:
            break
        x0 = x1
        dx = -f(x0)/J(x0)
        x1 = x0 + dx
        hist += [x1]
        i = i+1
    return (x1,hist)

def dampNewton(f,J,x,tol=1e-8,maxIter=0):
    """
    Gedämpfte Newtoniteration
    @param f: betrachtete Funktion
    @param J: Ableitung der betrachteten Funktion
    @param x0: Startwert
    @param tol: Abbruch bei Fehler < tol
    @param maxIter: Abbruch nach maxIter Iterationen
    """
    x0 = x
    dx = -f(x0)/J(x0)
    x1 = x0 + dx
    alpha = 1
    while np.abs(f(x1))>np.abs(f(x0)):
        if alpha < 1e-7:
            return np.complex(0,0)
        alpha = alpha/2
        x1 = x0 + alpha * dx
    i = 1
    while la.norm(x1 - x0) >= tol:
        if maxIter > 0 and i >= maxIter:
            break
        x0 = x1
        dx = -f(x0)/J(x0)
        x1 = x0 + dx
        alpha = 1
        while np.abs(f(x1))>np.abs(f(x0)):
            if alpha < 1e-7:
                return np.complex(0,0)
            alpha = alpha/2
            x1 = x0 + alpha * dx
        i = i+1
    return x1

def GaussNewton(f,J,x,tol=1e-8,maxIter=0):
    """
    Gauss-Newton-Iteration
    @param f: betrachtete Funktion
    @param df: Ableitung der betrachteten Funktion
    @param x0: Startwert
    @param tol: Abbruch bei Fehler < tol
    @param maxIter: Abbruch nach maxIter Iterationen
    """
    x0 = x
    dx = la.lstsq(J(x0),-f(x0))[0]
    x1 = x0 + dx
    i = 1
    while la.norm(x1 - x0) > tol:
        if maxIter > 0 and i >= maxIter:
            break
        x0 = x1
        dx = la.lstsq(J(x0),-f(x0))[0]
        x1 = x0 + dx
        i+=1
    return x1
    
def aitken(f, x, tol = 1e-8, maxIter = 0):
    """
    Aitkens epsilon-/Delta-Quadrat-Algorithmus
    @param f: betrachtete Funktion
    @param J: Ableitung der betrachteten Funktion
    @param x0: Startwert
    @param tol: Abbruch bei Fehler < tol
    @param maxIter: Abbruch nach maxIter Iterationen
    """
    x0 = x
    x1 = f(x0)
    x2 = f(x1)
    dx = x2 - x1
    ddx = x2 - 2 * x1 + x0
    x3 = x2 - dx**2 / ddx
    hist = [x,x3]
    while (la.norm(x3-hist[-2]) > tol):
        x0 = x1
        x1 = x2
        x2 = f(x1)
        dx = x2 - x1
        ddx = x2 - 2 * x1 + x0
        x3 = x2 - dx**2 / ddx
        hist += [x3]
    return (x3,np.array(hist))
    
    
    
    