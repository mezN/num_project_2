# -*- coding: utf-8 -*-
"""
Numerik 2, Projekt 2,
Aufgabe 2.1.1: Lineare Regression
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import matplotlib.pyplot as plt

# Im folgenden wollen wir einen funktionalen Zusammenhang zwischen Jahres-
# zahlen und gemeldeten Mobiltelefonen herstellen.

# Das Array D enthält den Datensatz, in der ersten Zeile befinden sich Jahreszahlen (X-Werte) 
# und in der zweiten Zeile die dazugehörige Anzahl von gemeldeten Mobiltelefonen (Y-Werte).
D = np.array([[1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
  1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2008, 2009, 2010],
 [340213, 681825, 1230855, 2069441, 3508944, 5283055, 7557148, 11032753,
  16009461, 24134421, 33758661, 44042992, 55312293, 69209321, 86047003, 
  109478031, 128374512, 140766842, 158721981, 182140362, 207896198, 233000000,
  262700000, 276610580, 300520098]], dtype = np.float)

# Wir verschieben die Jahreszahlen, sodass sie bei 0 beginnen.
D[0] = D[0]-1985

# Damit wir die nicht-lineare Modellfunktion: "y = f(x) = a*exp(b*x)" mittels linearer 
# Regression bestimmen können, logarithmieren wir zunächst die Daten und fügen sie als 
# weitere Zeile in unser Datenarray ein.
D = np.append(D, [np.log(D[1])], axis = 0)
# Zusätzlich fügen wir eine Zeile von Daten ein, die zuerst normalisiert und anschließend
# logarithmiert wurden.
D = np.append(D, [np.log(D[1]*1e-6)], axis = 0)

# Dadurch können wir den Zusammenhang "log y = log(a) + b*x" mittels einer linear-affinen 
# Funktion "f(x) = a' + b*x" modellieren. Die resultierende Datenmatrix A enthält in der 
# ersten Spalte die Auswertungen der Basisfunktion "x" an allen Eingabewerten und in der
# zweiten Spalte die Auswertungen der Basisfunktion "1".
A=np.vstack([D[0],np.ones(len(D[0]))]).T

# Die Parameter die den affinen Zusammenhang zwischen Jahreszahlen und logarithmierten gemeldeten Mobiltelefonen 
# beschreiben, können nun durch die Minimierung der Fehlerquadrate bestimmt werden. 
b1,log_a1 = np.linalg.lstsq(A,D[2])[0]
b2,log_a2 = np.linalg.lstsq(A,D[3])[0]

# Durch das Delogarithmieren der affinen Funktion erhalten wir die gewünschte Modellfunktion.
a1=np.exp(log_a1)
a2=np.exp(log_a2)

plt.plot(D[0],a1*np.exp(b1*D[0]), label = "a*exp(b*x)")
plt.plot(D[0],a2*np.exp(b2*D[0])*1e6, label = "a*exp(b*x), normalisiert")
plt.plot(D[0], D[1], label = "Daten")
plt.legend(loc=2)
plt.show()

"""
 Wie man am Plot unschwer erkennen kann, hat die Normalisierung der Daten keine 
 Auswirkung auf das Ergebnis, die Graphen beider Modellfunktionen liegen direkt
 übereinander.
"""

