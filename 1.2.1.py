"""
Numerik 2, Projekt 2,
Aufgabe 1.2: Newton-Fraktale
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import sys

# In dieser Aufgabe betrachten wir Newton-Fraktale beim Nullstellensuchen einer
# komplexen Funktion

# Wir weisen jedem Pixel die entsprechende komplexe Zahl auf der Gaußschen Zahlenebene zu,
# betrachten den Konvergenzwert des Newton-Verfahrens zu dieser Zahl und färben die Pixel
# dementsprechend ein.

# Python bzw. Numpy kann mit komplexen Zahlen umgehen.

def f(x):
    """
    die gegebene Funktion
    """
    return x**3-1
    
def J(x):
    """
    Analytisch berechnete Jacobi Matrix für die gewünschte Stelle.
    """
    return 3*x**2
    
if __name__ == "__main__":
    eps = sys.float_info.epsilon

    rng = np.arange(-1,1,0.01)
    # Das Grundgitter von komplexen Zahlen
    mesh = np.array([[np.complex(x,y) for x in rng] for y in rng])

    # Lasse auf jeden Punkt des Meshes das Newtonverfahren mit 40 Iterationen laufen
    # und betrachte die gefundenen Punkte
    x_opts = [s.newton1D(f,J,x0,np.sqrt(eps),40)[0] for x0 in mesh.flatten()]

    color = []

    # falls die Punkte näher als 10⁻³ an einem der Nullpunkte liegen, färbe die Ausgangspunkte
    # entsprechend ein. Andernfalls betrachte die Ausgangspunkte als nicht konvergent
    # (schwarz)
    for x in x_opts:
        if la.norm(x-1)<1e-3:
            # Die erste Einheitswurzel -> Blau
            color.append((0,0,1))
        elif la.norm(x-np.complex(np.cos(2*np.pi/3),np.sin(2*np.pi/3)))<1e-3:
            # Die zweite Einheitswurzel -> Rot
            color.append((1,0,0))
        elif la.norm(x-np.complex(np.cos(4*np.pi/3),np.sin(4*np.pi/3)))<1e-3:
            # Die dritte Einheitswurzel -> Grün
            color.append((0,1,0))
        else:
            # ansonsten -> Schwarz
            color.append((0,0,0))

    # Ordne die Farben in ein Doppelarray an (wie `plt.imshow` will)
    color=np.array(color).reshape((len(rng),len(rng),3))
    plt.imshow(color,extent=[-1,1,-1,1])
    plt.show()