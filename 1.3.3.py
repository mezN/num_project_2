"""
Numerik 2, Projekt 2,
Aufgabe 1.3: Konvergenzbeschleunigung des Newtonverfahrens von Aufgabe 1.1

@author: Chris Wendler, Franz Luggin, Jona Klemenc 
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
import sys


def f(x):
    """
    Berechnet für den 2x1-Vektor 'x' die in der Angabe gegebenen Gleichungen
    """
    fx = np.zeros((2,),dtype=np.float)
    fx[0] = x[0]**2 + x[1]**2 + 0.6*x[1] - 0.16
    fx[1] = x[0]**2 - x[1]**2 + x[0] - 1.6*x[1] - 0.14
    return fx
    
def J(x):
    """
    Analytisch berechnete Jacobi Matrix für die gewünschte Stelle.
    """
    Jx = np.zeros((2,2),dtype=np.float)
    Jx[0,0] = 2*x[0]
    Jx[0,1] = 2*x[1] + 0.6
    Jx[1,0] = 2*x[0] + 1
    Jx[1,1] = -2*x[1] - 1.6
    return Jx

if __name__ == "__main__":
    x_0 = np.array([0.6,0.25])      # gegebener Startwert
    eps = np.sqrt(sys.float_info.epsilon)   # Wurzel aus der Maschinengenauigkeit
    # Eine Iteration des Newtonverfahrens für s.aitken 
    phi = lambda x: s.newton(f,J,x,maxIter = 1)[0]

    (x_opt, hist) = s.newton(f,J,x_0,tol=eps)   # Newtonverfahren
    # Mit epsilon-Algorithmus beschleunigtes Verfahren
    (x_opt2,hist2) = s.aitken(phi,x_0,tol=eps)

    # Konvergenzverhalten
    err = np.array([la.norm(x_opt-x) for x in hist])
    err2 = np.array([la.norm(x_opt2-x) for x in hist2])
    # X-Achse: Anzahl der Iterationen
    # Y-Achse: Fehler
    plt.semilogy(err,label="Newton")
    plt.semilogy(err2,label="Aitken")
    plt.legend()
    plt.show()
    
    """
    Bei einer quadratischen Konvergenz ist der epsilon-Algorithmus weniger vorteilhaft,
    er verbessert das Ergebnis pro Schritt leicht, dafür ist jeder Schritt aufwändiger.
    Die Newton-Iteration konvergiert bereits quadratisch, daher ist auch die Voraussetzung
    der geometrischen Konvergenz für Aitken nicht mehr gegeben und der Algorithmus
    ist daher nicht effizient, will man mehr Präzision ist ein zusätzlicher Newton-Schritt
    effizienter.
    """
    
    
    
    