"""
Numerik 2, Projekt 2,
Aufgabe 1.1: Quadratische Konvergenz
@author: Franz Luggin, Jona Klemenc, Chris Wendler
"""

import numpy as np
import numpy.linalg as la
import solver as s
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import sys

# Wir untersuchen und veranschaulichen das Konvergenzverhalten unterschiedlicher Verfahren zur
# Nullstellensuche

def f(x):
    """
    Die linke Seite des gegebenen Gleichungssystems; als Funktion R² -> R²
    Eine Nullstelle der Funktion ist eine Lösung des Gleichungssystems
    """
    fx = np.zeros((2,),dtype=np.float)
    fx[0] = x[0]**2 + x[1]**2 + 0.6*x[1] - 0.16
    fx[1] = x[0]**2 - x[1]**2 + x[0] - 1.6*x[1] - 0.14
    return fx
    
def J(x):
    """
    Von Hand berechnete analytische Jacobi-Matrix für die gewünschte Stelle.
    """
    Jx = np.zeros((2,2),dtype=np.float)
    Jx[0,0] = 2*x[0]
    Jx[0,1] = 2*x[1] + 0.6
    Jx[1,0] = 2*x[0] + 1
    Jx[1,1] = -2*x[1] - 1.6
    return Jx

if __name__ == "__main__":
    # Wir erstellen zwei Plots: Einmal einen 3d-Plot mit der Funktion (x, y) -> ||f(x, y)||
    # sowie den Punkten der Newton-Iteration und einmal ein Diagramm des Fehlers
    # in Abhängigkeit der Anzahl der Iterationen

    # hist enthält einen Array der Werte, die das Newton-Verfahren bereits durchlaufen hat
    (x_opt, hist) = s.newton(f,J,np.array([0.6,0.25]),tol=np.sqrt(sys.float_info.epsilon))

    # Bereite die Daten für den ersten Plot auf
    X = np.arange(-0.5, 0.5, 0.003)
    X, Y = np.meshgrid(X, X)
    Z = np.array([la.norm(f(np.array([x,y]))) for x,y in zip(np.ravel(X),np.ravel(Y))])
    Z = Z.reshape(X.shape)

    # init mplot3d
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    surf = ax.plot_surface(X,Y,Z,cmap=cm.jet,linewidth=0.1,alpha = 0.67)

    # Füge die Punkte des Newton-Verfahrens hinzu
    X2 = np.array(hist).T[0]
    Y2 = np.array(hist).T[1]
    Z2 = np.array([la.norm(f(np.array([x,y]))) for x,y in zip(np.ravel(X2),np.ravel(Y2))])
    scatter = ax.scatter(X2,Y2,Z2,s = 100,marker = "x",c="r")
    plt.show()

    # Zeige den zweiten Plot: Abstand zum (gefundenen) Idealwert in Abhängigkeit
    # der Iterationen
    err = np.array([la.norm(x_opt-x) for x in hist])
    plt.semilogy(err)
    plt.show()

    # Ganz deutlich ist die quadratische Konvergenz zu sehen: Die Anzahl der stimmigen
    # Nachkommastellen verdoppelt sich in etwa pro Iterationsschritt